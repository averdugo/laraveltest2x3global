@component('mail::message')
# Introduction

Test for 2x3-Global.

@component('mail::button', ['url' => ''])
Hire
@endcomponent

Thanks,<br>
Aldo Verdugo Carrasco
@endcomponent
