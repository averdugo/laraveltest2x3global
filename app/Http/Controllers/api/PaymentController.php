<?php

namespace App\Http\Controllers\api;

use App\Client;
use App\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Jobs\StoreDollar;
use Carbon\Carbon;

class PaymentController extends ApiResponseController
{
    public function list($client_id){

        $clients = Client::find($client_id);
        return $this->successResponse($clients->payments);
        
    }

    public function create(Request $request) {
        
        $payment = Payment::create([
            'uuid' => Str::uuid(),
            'user_id' =>  $request->user_id,
            'expires_at' => $request->expires_at,
            'status' => $request->status,
        ]);
        
        StoreDollar::dispatch($payment);
        return $this->successResponse($payment);
        
    }
}
