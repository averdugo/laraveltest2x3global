<?php

namespace App\Http\Controllers\api;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientController extends ApiResponseController
{
    public function list(){

        $clients = Client::get();
        return $this->successResponse($clients);

    }
}
