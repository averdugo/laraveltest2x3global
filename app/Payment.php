<?php

namespace App;
use App\Events\PaymentEvent;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'uuid','user_id', 'expires_at', 'status', 'clp_usd', 'payment_date'
    ];

    public $timestamps = false;

    protected $dispatchesEvents = [
        'created' => PaymentEvent::class
    ];

}
