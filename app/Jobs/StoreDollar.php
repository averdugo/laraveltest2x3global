<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Payment;
use Carbon\Carbon;

class StoreDollar implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $payment;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $now = Carbon::now();
        $payment = $this->payment;
        $todayPayment = Payment::where([['payment_date', $now->toDateString()], ['clp_usd','<>', null]])->first();
        
        if(!$todayPayment) {

            $endpoint = "https://mindicador.cl/api/dolar";
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $endpoint);
            $content = json_decode($response->getBody(), true);

            $dt = new Carbon( $content['serie'][0]['fecha']);
            $payment->clp_usd = $content['serie'][0]['valor'];
            $payment->payment_date = $dt->toDateString();

        }else {
            
            $payment->clp_usd = $todayPayment->clp_usd;
            $payment->payment_date = $todayPayment->payment_date;

        }
        $payment->save();

       
    }
}
