<?php

use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clients = array(
            array(
                'email' => 'juan@gmail.com',
                'join_date' => '2020-05-20'
            ),
            array(
                'email' => 'carlos@gmail.com',
                'join_date' => '2020-05-10'
            ),
            array(
                'email' => 'pedro@gmail.com',
                'join_date' => '2020-05-12'
            ),
            array(
                'email' => 'riquelme@gmail.com',
                'join_date' => '2020-05-03'
            )
        );
        foreach ($clients as $key => $client) {
            DB::table('clients')->insert([
                'email' => $client['email'],
                'join_date' => $client['join_date'],
            ]);
        };

    }
}
